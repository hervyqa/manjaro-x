# MANJARO X 18.1

homepage : http://hervyqa.com/manjaro-x-gnome-18.1-64-bit/

Bismillahirrohmanirrohim.

GNU/Linux Manjaro-X dibuat untuk pengguna GNOME dengan rilis baru dan stabil, diciptakan khusus untuk Pengguna Awam, Desainer, Animator, Penyunting Film, dan Developer GTK. Manjaro-X juga dikembangkan dari manjaro-tools edisi GNOME. Perbedaannya pada beberapa paket yang sengaja dihilangkan seperti tema Adapta-matcha, branding manjaro yang berlebihan, warna hijaunya yang khas, atau beberapa hal yang "tidak terlihat seperti GNOME" akan dihilangkan. Dengan ini akan mengembalikan _cita rasa_ GNOME itu sendiri.

Tujuan dari Manjaro-X adalah membangun Distro Manjaro dengan lingkungan desktop GNOME. Pengembang ingin memudahkan penggunanya. Pengguna hanya memasang Manjaro-X sekitar 15 menit kemudian pengguna bisa langsung melupakannya, tidak perlu repot untuk memasang codec, tidak perlu memasang aplikasi pemutar musik dan video, tidak perlu memasang aplikasi pengembangan, aplikasi perkantoran, ataupun aplikasi desain tambahan. "Tinggal Pasang dan Pakai", Sesederhana itu dan begitu mudah.

# Unduh Manjaro-X

Manjaro-X hanya tersedia dengan arsitektur x86_x64. Klik [Unduh Manjaro-X](https://osdn.net/projects/manjaro-x/downloads/71776/manjaro-x-gnome-18.1.0-stable-x86_64.iso/).

***
# GNOME APPS

## GNOME 3.34.1 dan Kernel 4.19 

Manjaro-X sudah berisi beberapa aplikasi GNOME, diantaranya :

 - **Boxes** : Mesin Virtualisasi dan jarak jauh yang sederhana.
 - **Brasero** : Pembakar CD/DVD.
 - **Builder** : IDE dan Pengembang Aplikasi GTK.
 - **Cheese** : Kamera WebCam.
 - **Disk** : Utilitas manajemen partisi.
 - **Deja Dup** : Pencadangan dan Menjaga data-data penting.
 - **Glade** : Perancang Desain UI/UX bagi Aplikasi GTK.
 - **Epiphany** : Penjelajah Web.
 - **Empathy** : Layanan Obrolan.
 - **Evolution** : Pengolah Surat Elektronik.
 - **FeedReader** : RSS Klien.
 - **Fragment** : Klien BitTorrent.
 - **gThumb** : Penampil Gambar.
 - **Hexchat** : Klien Internet Relay Chat (IRC).
 - **Multiwriter** : Penulis berkas ISO ke beberapa perangkat sekaligus.
 - **Notes** : Penulis catatan sederhana.
 - **Peek** : Perekam GIF.
 - **Polari** : Klien Internet Relay Chat (IRC).
 - **Pitivi** : Penyunting Video.
 - **Rhythmbox** : Pemutar musik.
 - **Todo** : Manajer tugas.
 - **Totem** : Pemutar Video.
 - **Tilix** : Terminal

Aplikasi Hebat lainnya :

 - **Blender** : Pengolah Animasi dan Penyunting Video.
 - **Dconf** : Penyunting basis data dconf.
 - **Gimp** : Pengolah Bitmap.
 - **Inkscape** : Pengolah Vektor.
 - **LibreOffice** : Alat Perkantoran. (Impress, Writer, Calc, Draw, & Base)
 - **Steam** : Permainan dari Valve.
 - **TimeShift** : Pencadangan Data.
 - **Telegram** : Perpesanan Modern.
 - **Tweak-tool** : Pengaturan lanjutan.
 - **Uget** : Pengunduh banyak berkas.
 - **Gigle** : Git GUI
 - **Klavaro** : Learn typing
 - **Gnac** : Audio converter
 
Perkakas CLI :

 - **Tor** : Jalur jaringan TOR.
 - **Hugo** : Generator web statis.
 - **Youtube-dl** : Pengunduh Video.

***
# Pengaturan Sederhana.

Beberapa fungsi gsetting dirubah untuk memudahkan interaksi pengguna.

 - Klik Tap untuk Touchpad Aktif secara baku.
 - Penyortiran direktori di nautilus didahulukan.
 - Menu lebih ringan, karena hanya mengaktifkan fitur pencarian Aplikasi, Kontak dan kalkulator.
 - Persentase baterai aktif.
 - Mengkatifkan layout panel minimize, maximize, dan close.
 - Pintasan Rumah : `Super + E`.
 - Pintasan Web : `Super + W`.
 - Wallpaper Baku `Adwaita.xml`.
 - Pengaturan Layout Inkscape.
 - Pengaturan Layout Gimp.
 - Pengaturan Layout Blender 2.8.

***
# Tema gelap.

 Tema gelap dari Adwaita-Dark secara baku. Anda bisa mengganti mode cerah dengan menekan perintah Alt+F2 kemudian mengetik `lightmode`, atau kembali ke mode petang dengan mengetik `darkmode`.
 
 Mode malam aktif secara baku. Fitur ini berlaku untuk jam sore hingga terbit fajar. Mode malam akan mengurangi cahaya biru sehingga mata tetap aman dan nyaman.
 
***
# Mudah memutar Video dan Audio.

 Sudah terpasang codec multimedia. Memutar format video dan audio jadi lebih mudah dan tanpa memasang codec tambahan.
 
***
# Cara membangun Manjaro-X

- Update `sudo pacman -Syyu`
- Pasang `sudo pacman -S manjaro-tools-iso manjaro-iso-profiles-base`
- Duplikat `git clone git@gitlab.com:hervyqa/manjaro-x.git`
- Masuk folder `cd manjaro-x`
- Bangun Manjaro-x dengan GNOME profil `buildiso -p gnome`

***
# Contact

Telegram : t.me/hervyqa
